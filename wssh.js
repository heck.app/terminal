class WSSH {
	constructor(term, url) {
		this.term = term;
		this.ws = new WebSocket(url);
		ws.onopen = function() { term.write("websocket opened\n"); }
		ws.onclose = function() { term.write("websocket closed\n"); }
		ws.onerror = function() { term.write("websocket error\n"); }
		ws.onmessage = function() { term.write("websocket message\n"); }
		term.onkepress = function(ev) {
		};
		term.onkeydown = function(ev) {
		};
	}
};
