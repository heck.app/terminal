class Terminal {
	DCSM = "PRESENTATION";
	SIMD = 0;

	line_home = 0;
	line_limit = 80;
	page_home = 0;
	page_limit = 24;
	
	constructor(elementid) {
		let terminal = this;
		this.element = document.getElementById(elementid);
		this.queue = undefined;
		this.oninput = undefined;

		function enqueue(term, code) {
			if (!term.queue) {
				term.queue = code;
			} else {
				term.queue.append(code);
			}
			if (term.oninput) {
				term.oninput();
			}
			return false;
		}

		this.element.ontextinput = function(ev) { return false; }
		this.element.onkeyup = function(ev) { return false; }
		this.element.onkeypress = function(ev) {
			console.log("keypress:" + ev);
			return enqueue(terminal, ev.keyCode);
		}

		this.element.onkeydown = function(ev) {
			console.log(ev);
			if (ev.code == "Backspace") {
				return enqueue(terminal, 0x08);
			} else if (ev.code == "Enter") {
				return enqueue(terminal, 0x0a);
			}
			return true;
		}

		this.element.tabindex = 0;

		function create(lines, columns) {
			var div = document.createElement("div");
			div.x = 0;
			div.y = 0;
			div.style.width = columns + "ch";

			for (var y = 0; y < lines; y++) {
				var line = document.createElement("div");
				line.style.width = columns + "ch";
				for (var x = 0; x < columns; x++) {
					var cell = document.createElement("span");
					cell.innerText = " ";
					line.append(cell);
				}
				div.append(line);
			}

			return div;
		}
		this.PRESENTATION = create(this.page_limit, this.line_limit);
		this.DATA = create(this.page_limit, this.line_limit);
		this.DATA.style.display = "none";

		this.element.append(this.PRESENTATION);
		this.element.append(this.DATA);
		this.element.style.width = this.line_limit + "ch";
	}

	write(code) {
		return this.write_code(code);
		/*
		for (var i = 0; i < code.length; i++) {
			this.write_code(code.charCodeAt(i));
		}
		return false;
		*/
	}

	read() {
		var data = this.queue;
		this.queue = undefined;
		return data;
	}

	write_code(code) {
		if (code < 0x20) {
			console.log("0x" + code.toString(16));
		}
		switch (code) {
		case 0x00:	return this.NUL();
		case 0x01:	return this.SOH();
		case 0x02:	return this.STX();
		case 0x03:	return this.ETX();
		case 0x04:	return this.EOT();
		case 0x05:	return this.ENQ();
		case 0x06:	return this.ACK();
		case 0x07:	return this.BEL();
		case 0x08:	return this.BS();
		case 0x09:	return this.HT();
		case 0x0a:	return this.LF();
		case 0x0b:	return this.VT();
		case 0x0c:	return this.FF();
		case 0x0d:	return this.CR();
		case 0x0e:	return this.SO();
		case 0x0f:	return this.SI();

		case 0x10:	return this.DLE();
		case 0x11:	return this.DC1();
		case 0x12:	return this.DC2();
		case 0x13:	return this.DC3();
		case 0x14:	return this.DC4();
		case 0x15:	return this.NAK();
		case 0x16:	return this.SYN();
		case 0x17:	return this.ETB();
		case 0x18:	return this.CAN(); 
		case 0x19:	return this.EM();
		case 0x1a:	return this.SUB();
		case 0x1b:	return this.ESC();	///////////
		case 0x1c:	return this.FS();
		case 0x1d:	return this.GS();
		case 0x1e:	return this.RS();
		case 0x1f:	return this.US();
		}

		var span = this[this.DCSM].children[this.Y()].children[this.X()];
		span.innerText = String.fromCharCode(code);

		if (++this[this.DCSM].x >= this.line_limit) {
			this.CR();
			this.NL();
		}

		return false;
	}

	X() {
		if (this[this.DCSM].x < this.line_home) {
			this[this.DCSM].x = this.line_home;
		} else if (this[this.DCSM].x >= this.line_limit) {
			this[this.DCSM].x = this.line_limit - 1;
		}
		return this[this.DCSM].x;
	}

	Y() {
		if (this[this.DCSM].y < this.page_home) {
			this[this.DCSM].y = this.page_home;
		} else if (this[this.DCSM].y >= this.page_limit) {
			this[this.DCSM].y = this.page_limit - 1;
		}
		return this[this.DCSM].y;
	}

	ACK() { }
	APC() { }
	BEL() { }
	BPH() { }

	BS() {
		this[this.DCSM].x += this.SIMD ? +1 : -1;
	}

	CNL() { }
	CPL() { }
	CPR() { }

	CR() {
		this[this.DCSM].x = this.SIMD ? this.line_limit : this.line_home;
	}

	CUU() { }
	CVT() { }
	DA() { }
	DAQ() { }
	DC3() { }
	DC4() { }
	DL() { }
	DLE() { }
	DM1() { }
	ECH() { }
	ED() { }
	EF() { }
	ENQ() { }
	EOT() { }
	EPA() { }
	ESA() { }
	ESC() { }
	ETB() { }
	ETX() { }
	GSM() { }
	GSS() { }
	HPA() { }
	HPB() { }
	HPR() { }
	HT() { }
	IDCS() { }
	IGS() { }
	IL() { }
	LF() { }
	LS0() { }
	LS1() { }
	LS1R() { }
	LS2() { }
	LS2R() { }
	NEL() { }

	NL() {
		this[this.DCSM].y++;
	}

	NP() { }

	NUL() {
		/* NOOP */
	}

	OSC() { }
	PEC() { }
	PLU() { }
	PM() { }
	PP() { }
	PPA() { }
	PPB() { }
	PPR() { }
	PTX() { }
	QUAD() { }
	REP() { }
	RI() { }
	RIS() { }
	RM() { }
	SACS() { }
	SCI() { }
	SCO() { }
	SCP() { }
	SEE() { }
	SEF() { }
	SGR() { }
	SLH() { }
	SLL() { }
	SLS() { }
	SO() { }

	SOH() {
		/* See ISO 1745 */
	}

	SOS() { }
	SPA() { }
	SPD() { }
	SPL() { }
	SPQR() { }
	SR() { }
	SRCS() { }
	SSW() { }
	SS2() { }
	SS3() { }
	ST() { }
	SYN() { }
	TAC() { }
	TALE() { }
	TATE() { }
	VPA() { }
	VPB() { }
	VPR() { }
	VT() { }
	VTS() { }

};
